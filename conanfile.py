from conan import ConanFile

class Mar345Recipe(ConanFile):
    name = "mar345"
    executable = "ds_Mar345"
    version = "1.4.0"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Florent Langlois"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/acquisition/2d/mar345.git"
    description = "Mar345Device device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
