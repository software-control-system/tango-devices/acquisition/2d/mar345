// ============================================================================
//
// = CONTEXT
//		DeviceTask attached to the Mar345 device,
//
// = File
//		Mar345Task.cpp
//
// = AUTHOR
//		FL - SOLEIL
//
// ============================================================================
#include <yat/threading/Mutex.h>
#include <Mar345Task.h>
#include <yat/time/Timer.h>
#include <mar345_header.h>

//- Get marresearch (Klein) code
extern "C" {MAR345_HEADER Getmar345Header(FILE * file);}
extern "C" {int readmar	( char* file);}
extern unsigned short* i2_image;


namespace Mar345_ns {
//- ctor
Mar345Task::Mar345Task(Tango::DeviceImpl* dev) :
	yat4tango::DeviceTask(dev), m_status("INIT"),m_state(Tango::INIT) 
{
	INFO_STREAM << "Mar345Task::Mar345Task"<< endl;

	enable_timeout_msg(false);
	m_periodic_period_ms = 500;
	set_periodic_msg_period(m_periodic_period_ms);
	enable_periodic_msg(false);
	m_is_logs_available = false;
	m_files_init_ok = true;
	m_is_image_available = false;
    mar_serial_number_line = "Not found";
}

//- dtor
Mar345Task::~Mar345Task() 
{
	INFO_STREAM << "Mar345Task::~Mar345Task"<< endl;
}

//- Process the message queue
void Mar345Task::process_message(yat::Message& msg) throw (Tango::DevFailed) 
{
	try
	{
		switch ( msg.type() )
		{
			//-----------------------------------------------------------------------------------------------
			case yat::TASK_INIT:
			{
				INFO_STREAM << "-> yat::TASK_INIT" << endl;
				try
				{
					m_mar_conf = msg.get_data<MarConfig>();

					m_image_values.reset(new ImageValues);
					m_logs.reset(new Logs);

					//- Opened later (SCAN/ERASE..)
					MAR_COM_FILE_NAME 		= m_mar_conf.mar_com_file_name;
					//- Opened at init
					MAR_SPY_FILE_NAME 		= m_mar_conf.mar_spy_file_name;
					MAR_PROGRESS_FILE_NAME 	= m_mar_conf.mar_progress_file_name;
					MAR_NUM_FILE_NAME		= m_mar_conf.mar_num_file_name;
					
					//-  Get Mar Serial NO
					ifstream mar_num_file_file; 
					mar_num_file_file.open((const char*)MAR_NUM_FILE_NAME.c_str());
					if(!mar_num_file_file.fail())
					{
                        string line;
                        while (getline(mar_num_file_file, line))
                        {
                            INFO_STREAM << "line : " << line << endl;
                            if (line.find_first_of("#") != 0)
                                mar_serial_number_line = line;
                        }

                        INFO_STREAM << "MAR SERIAL NUM : " << mar_serial_number_line << endl;
                        mar_num_file_file.close();
					}
					else
					{
						m_files_init_ok = false;
						ERROR_STREAM << "Mar num file: " << MAR_NUM_FILE_NAME << " does not exist" << endl;
					}

					//- Progress and Spy Files
					//-----------
					spyfile.open((const char*)MAR_SPY_FILE_NAME.c_str(),ios_base::ate); //- go at end of the file;
					if(!spyfile.fail() && spyfile.is_open())
					{
						INFO_STREAM << "Spy file opened : " << MAR_SPY_FILE_NAME << endl;
					}
					else
					{
						m_files_init_ok = false;
						ERROR_STREAM << "Spy file: " << MAR_SPY_FILE_NAME << " cannot be opened" << endl;
					}
					//-----------
					progressfile.open((const char*)MAR_PROGRESS_FILE_NAME.c_str());
					if(!progressfile.fail())
					{
						INFO_STREAM << "Progress file opened : " << MAR_PROGRESS_FILE_NAME << endl;
					}
					else
					{
						m_files_init_ok = false;
						ERROR_STREAM << "Progress file: " << MAR_PROGRESS_FILE_NAME << " does not exist" << endl;
					}

					//--
					if(m_files_init_ok == true)
					{
						m_state = Tango::STANDBY;
						m_status = "MAR345 is Standby: waiting for request...";
					}
					else
					{
						m_state = Tango::FAULT;
						m_status = "MAR345 is Fault: check com/spy/messages files ...";
					}
				}
				catch(std::exception& e)
				{
					ERROR_STREAM <<e.what()<<endl;
					m_state = Tango::FAULT;
					m_status= e.what();					
				}
				catch( Tango::DevFailed& df )
				{
					ERROR_STREAM << df <<endl;
					m_state = Tango::FAULT;
					m_status= string(df.errors[df.errors.length()-1].desc);
				}
			}
			break;
				
			//-----------------------------------------------------------------------------------------------
			case yat::TASK_EXIT:
			
				INFO_STREAM << "-> yat::TASK_EXIT" << endl;
				m_state = Tango::STANDBY; 

				spyfile.close();
				comfile.close();
				progressfile.close();

				m_image_values.reset();
				m_logs.reset();


			break;

			//-----------------------------------------------------------------------------------------------
			case SCAN_MSG:
			
				INFO_STREAM << "-> yat::SCAN_MSG" << endl;
				m_state = Tango::RUNNING;				
				m_status= "MAR345 is Scanning..."; 	

				command.str("");
				m_erase_asked = false;
				m_is_image_available = false;
				m_logs->spy_logs.clear();
				m_logs->progress_logs.clear();

				m_mar_conf = msg.get_data<MarConfig>();

				//- Write commands in COMMAND File
				//-
				comfile.open((const char*)MAR_COM_FILE_NAME.c_str());
				command << "FORMAT " << m_mar_conf.format << endl;
				//- will be: "ROOT nomfichier_006"
				command << "ROOT " << m_mar_conf.root << "_" << std::setfill ('0') << std::setw (3) << m_mar_conf.image_number << endl;
				command << "MODE " << m_mar_conf.mode << endl;
				command << "DIRECTORY " << m_mar_conf.dir << endl;
				
				//- Start the scan
				command << "COMMAND SCAN" << endl;
				INFO_STREAM << "Command lines: " << command.str() << endl;
				comfile << command.str();
				chrono.restart();
				comfile.close();
				
				end_str 		= "SCAN_DATA    Ended o.k.    TASK SCAN_DATA";
				end_str_error 	= "SCAN_DATA    Errorending   TASK SCAN_DATA";

				//- Pour marsim
				//end_str = "scan345: Task SCAN ENDED";
				//image_number += 1;


				//- Re open the spy and progress files, to be sure that we can reach them
				if(!spyfile.is_open())
				{

					spyfile.open((const char*)MAR_SPY_FILE_NAME.c_str(),ios_base::ate); //- go at end of the file;
					if(!spyfile.fail() && spyfile.is_open())
					{
						INFO_STREAM << "Spy file opened : " << MAR_SPY_FILE_NAME << endl;
					}
					else
					{
						m_files_init_ok = false;
						ERROR_STREAM << "Spy file: " << MAR_SPY_FILE_NAME << " cannot be opened" << endl;
					}
				}
				if(!progressfile.is_open())
				{
					//-----------
					progressfile.open((const char*)MAR_PROGRESS_FILE_NAME.c_str());
					if(!progressfile.fail())
					{
						INFO_STREAM << "Progress file opened : " << MAR_PROGRESS_FILE_NAME << endl;
					}
					else
					{
						m_files_init_ok = false;
						ERROR_STREAM << "Progress file: " << MAR_PROGRESS_FILE_NAME << " does not exist" << endl;
					}
				}
				//--
				if(m_files_init_ok == false)
				{
					m_state = Tango::ALARM;
					m_status = "MAR345 is Alarm: status files are not reachables: you will not be warn for the end of Scanning \n Or the Scan has not been Started (re start the whole system)";
				}
				else
				{
					//- start the periodic loop
					enable_periodic_msg(true);
				}

			break;

			//----------------------------------------------------------------------------------------------- 
			case yat::TASK_PERIODIC:
			
				//INFO_STREAM << "-> yat::TASK_PERIODIC" << endl;
				try
				{		
					//- READ SPY file and check line
					//---------------------------------------
					string spyline;
					getline(spyfile,spyline);
					if (spyline != "")
					{
						m_logs->spy_logs.push_back(spyline);
						INFO_STREAM << "spyline : " << spyline << endl;
					}
					spyfile.clear(); //- reset the status flags (eof...)
					
					string progressline;
					getline(progressfile,progressline);
					if (progressline != "")
					{
						m_logs->progress_logs.push_back(progressline);
						INFO_STREAM << "progressline : " << progressline << endl;
					}
					progressfile.clear(); //- reset the status flags (eof...)
						
					//-----				
					if ((spyline.find(end_str)!= string::npos ) || (spyline.find(end_str_error)!= string::npos ))
					{
						INFO_STREAM << "Finished !!!!!!!!!!!!!!!!!!!!!" << endl;
						enable_periodic_msg(false);

						if(!m_erase_asked) //- Scan command
						{
							if(m_mar_conf.get_image == true)
							{
								yat::Message* msg = yat::Message::allocate( READ_FILE_MSG, DEFAULT_MSG_PRIORITY, false );
								msg->attach_data( m_mar_conf );
								post(msg);
								return;
							}
						}
						//- 
						m_state = Tango::STANDBY;
						m_status = "MAR345 is Standby: waiting for request...";
						INFO_STREAM << "Action on Mar345 ended in " << chrono.elapsed_sec() << " sec." << endl;
					}						
				}
				catch(Tango::DevFailed &df)
				{	
					ERROR_STREAM << df << endl;
					m_state = Tango::ALARM;
					m_status = string(df.errors[0].desc);
					//is_data_available_ = false;
				}
			break;

			//----------------------------------------------------------------------------------------------- 
			case ERASE_MSG:

				INFO_STREAM << "-> yat::ERASE_MSG" << endl;
				m_state = Tango::MOVING;				
				m_status= "MAR345 is Erasing..."; 	

				command.str("");
				m_erase_asked = true;
				m_is_image_available = false;
				m_logs->spy_logs.clear();
				m_logs->progress_logs.clear();

				try
				{
					//- Write erase COMMAND
					comfile.open((const char*)MAR_COM_FILE_NAME.c_str());
					command << "IPS 12 0 16"; //- MANTIS 15882 From C Klein: means: the scanner will do a scan without transferring data. (do a better Erase than "COMMAND ERASE")
					comfile << command.str();
					chrono.restart();
					comfile.close();

					end_str			= "SCAN_ERASE   Ended o.k.    TASK SCAN_ERASE";
					end_str_error 	= "SCAN_ERASE    Errorending   TASK SCAN_ERASE";

					//- because of use of IPS 10...
					//end_str		= "SCAN_DATA    Ended o.k.    TASK SCAN_DATA";
					//end_str_error 	= "SCAN_DATA    Errorending   TASK SCAN_DATA";

					//- Will start the periodic loop
					enable_periodic_msg(true);

				}
				catch(Tango::DevFailed &df)
				{
					ERROR_STREAM << df << endl;
					m_state = Tango::ALARM;
					m_status = string(df.errors[0].desc);
				}
			break;

			//----------------------------------------------------------------------------------------------- 
			case READ_FILE_MSG:

				INFO_STREAM << "-> yat::READ_FILE_MSG" << endl;

				m_mar_conf = msg.get_data<MarConfig>();

				try
				{
					//- Open/Close file to get info from header (only for logs)
					FILE* file;
					file = fopen((char*)m_mar_conf.data_file_name.c_str(),"r+");
					MAR345_HEADER	h345;
					h345 = Getmar345Header(file);
					fclose( file );		

					INFO_STREAM << "Format				= "	<< (int)h345.format	<< std::endl;
					INFO_STREAM << "SerialNo			= "	<< h345.scanner	<< std::endl;
					INFO_STREAM << "Number of high pix	= " << h345.high	<< std::endl;
					INFO_STREAM << "Side (square)		= "	<< h345.size	<< std::endl;
								
					//----
					m_image_values->values.clear();
					//- maybe not usefull:
					m_image_values->values.reserve(h345.size * h345.size);
					m_image_values->width = h345.size;
					m_image_values->height = h345.size;

					//- Read the file and depack it (thanks to Klein)
					readmar((char*)m_mar_conf.data_file_name.c_str());

					{
						yat::MutexLock scoped_lock(m_image_lock);
					
						for (int i =0; i<(h345.size * h345.size); i++)
						{
							//- Ca marche!!!
							m_image_values->values.push_back(i2_image[i]);
						}
					}
					//- Free the i2_image pointer that come from readmar code
					if(i2_image != NULL)
					{
						free(i2_image);
						i2_image = NULL;
					}

					m_is_image_available = true;

					m_state = Tango::STANDBY;
					m_status = "MAR345 is Standby: waiting for request...";
					INFO_STREAM << "Action on Mar345 (with read file) ended in " << chrono.elapsed_sec() << " sec." << endl;
				}
				catch(...)
				{
					ERROR_STREAM << "Unknwon Error in trying to read image file: " << m_mar_conf.data_file_name << endl;
				}

			break;
		}
	}
	catch( yat::Exception& ex )
	{
		//- TODO Error Handling
		throw;
	}
}

// ============================================================================
// Mar345Task::get_logs
// ============================================================================
yat::SharedPtr<Logs> Mar345Task::get_logs(void) 
{
	yat::SharedPtr<Logs> logs;
	{
		yat::MutexLock scoped_lock(m_m_logslock);
		logs = m_logs;
	}

	return logs;
}

// ============================================================================
// Mar345Task::get_image_values
// ============================================================================
yat::SharedPtr<ImageValues> Mar345Task::get_image_values(void) 
{
	chrono.restart();
	//ensure that image is avalaible
	if (!m_is_image_available)
	{
		THROW_DEVFAILED("NO_DATA_ERROR",
						"No image is available",
						"Mar345Task::get_image_values");
	}
	//- to allow only 1 get_image_values
	m_is_image_available = false;
	yat::SharedPtr<ImageValues> image_values;
	{
		yat::MutexLock scoped_lock(m_image_lock);
		image_values = m_image_values;
	}

	INFO_STREAM << "get_image_values ended in :" << chrono.elapsed_msec() << " msec." << endl;
	return image_values;
}

// ============================================================================
// Mar345Task::get_mar_serial_number
// ============================================================================
string Mar345Task::get_mar_serial_number(void) 
{
	return mar_serial_number_line;
}

// ============================================================================
// Mar345Task::get_state
// ============================================================================
Tango::DevState Mar345Task::get_state(void) 
{
	yat::MutexLock scoped_lock(m_m_statuslock);
	return m_state;	
}


// ============================================================================
// Mar345Task::get_status
// ============================================================================
std::string Mar345Task::get_status (void)
{   
	yat::MutexLock scoped_lock(m_m_statuslock);
    return m_status;
}

// ============================================================================

}
