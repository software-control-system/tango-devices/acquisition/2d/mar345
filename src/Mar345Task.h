// ============================================================================
//
// = CONTEXT
//		DeviceTask attached to the MAR345 device,
//
// = File
//		Mar345Task.h
//
// = AUTHOR
//		FL - SOLEIL
//
// ============================================================================
#ifndef _MAR_TASK_H_
#define _MAR_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat/threading/Mutex.h>
#include <yat4tango/DeviceTask.h>
#include <yat/memory/SharedPtr.h>
#include <iostream>
#include <iomanip>
#include <fstream>





// ============================================================================
// DEVICE TASK ACTIVITY PERIOD IN MILLISECS
// ============================================================================
//- the following timeout set the frequency at which the task generates its data
#define kTASK_PERIODIC_TIMEOUT_MS 1000
#define THRESHOLD_DIRECTION_SIGNAL 2000
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
const size_t SCAN_MSG 		= yat::FIRST_USER_MSG + 0;
const size_t ERASE_MSG		= yat::FIRST_USER_MSG + 2;
const size_t READ_FILE_MSG	= yat::FIRST_USER_MSG + 3;


//--
namespace Mar345_ns
{

	struct Logs
	{
		std::vector<string>	spy_logs;
		std::vector<string>	progress_logs;
	};

	struct ImageValues
	{
		int width;
		int height;
		vector<unsigned int> values;
	};

  class Mar345Task : public yat4tango::DeviceTask
  {

  public:
	//- [ctor]
    Mar345Task( Tango::DeviceImpl* dev );
	//- [dtor]
    ~Mar345Task();
	//- returns the last state of the task ------
    Tango::DevState get_state(void);
	//- returns the last status of the task ------
	std::string get_status(void); 

	//- Get data form the task to the device
	yat::SharedPtr<Logs> get_logs(void);
	yat::SharedPtr<ImageValues> get_image_values(void);
	string get_mar_serial_number(void) ;

	struct MarConfig
	{
		string mar_com_file_name;
		string mar_spy_file_name;
		string mar_progress_file_name;
		string mar_num_file_name;
		string data_file_name;
		//-
		string format;
		string root;
		short image_number;
		short mode;
		string dir;
		//-
		float distance;
		double wavelength;
		double phiStart,phiEnd,phiOscillations;
		double chi;
		double twotheta;
		double exposureTime;
		string comments;
		double slitGapX,slitGapZ;
		//-
		bool get_image;

	};

  protected://- [yat4tango::DeviceTask implementation]
    virtual void process_message( yat::Message& msg ) throw (Tango::DevFailed);

  private:
    //- Struct containing the Task configuration
	MarConfig m_mar_conf;      
	
	//- Task Stuff
	yat::Mutex m_m_logslock,m_image_lock,m_m_statuslock;
    std::string m_status;
    Tango::DevState m_state;
	size_t m_periodic_period_ms;

	//- local data for passing to the device thread
	yat::SharedPtr<Logs> m_logs;
	bool m_is_logs_available;
	yat::SharedPtr<ImageValues> m_image_values;
	bool m_is_image_available;

	bool m_files_init_ok;
	bool m_erase_asked;

	//- MAR Stuff
	ifstream spyfile,progressfile;
	ofstream comfile;
	stringstream command;
	string old_line;
	string end_str; 		
	string end_str_error;
	vector<string> marLogs;
	string MAR_COM_FILE_NAME;
	string MAR_SPY_FILE_NAME;	
	string MAR_PROGRESS_FILE_NAME;
	string MAR_NUM_FILE_NAME;
	string mar_serial_number_line;
	yat::Timer chrono;
	
	

  };
  
  //-------------------------------------------------------------
  //- Functor called when reset() the object SRCDAcquisitionTask
  struct TaskExiter
  {
	void operator ()(yat4tango::DeviceTask* t)
	{
		try
		{
			//- Automatically post the TASK_EXIT msg
			cout << "TaskExiter..." << endl;
			t->exit();
		}
		catch(...)
		{
		}
	}
  };
  
  
}


#endif //_MAR_TASK_H_


