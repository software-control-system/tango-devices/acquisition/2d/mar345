Mar345 C++ Tango DeviceServer (Synchrotron Soleil F. Langlois)
================================

- The device uses:
- a yat task to wait the end of scanning/erasing
- boost shared_ptr
- the DiffractionImage library (http://www.ccp4.ac.uk/cvs/viewvc.cgi/ccp4/lib/DiffractionImage/?sortby=date#dirlist) to open the created .mar file, and put it in the image attribute
- it currently compile only on linux
