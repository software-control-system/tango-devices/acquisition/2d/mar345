/***********************************************************************
 *
 * Program: readmar 	Read an image in mar345 format
 *
 * Copyright by:        Dr. Claudio Klein
 *                      Marresearch GmbH, Hamburg
 *
 * Version:     	2.0
 *
 * Version	Date		Description
 * 2.0		29/08/2007	Modifications for mar555 images
 * 1.0		24/06/1999	Original version
 *
 ***********************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <mar300_header.h>
#include <mar345_header.h>

//#include <string>




#define VERSION 	2.0
#define FPOS(a) 	( (int)( a/8. + 0.875 )*64)
#define N		65535
#define WORD short int

char 			file		[  80] = {"\0"}; 
char 			str		[1024];
char			byteswap	= 0;

FILE 			*in, *fplp;

int			nx, ny, no;
unsigned short 		*i2_image=NULL;
unsigned char  		*ch_image=NULL;

MAR300_HEADER		h300;
MAR345_HEADER		h345;

int 			readmar	( char* file);
static void		usage		(void);

extern MAR345_HEADER    Getmar345Header(FILE *);
extern MAR300_HEADER    Getmar300Header(FILE *);

/*==================================================================*/
int readmar( char* file)
{
int 		i,j;
int		h1,iadd;
int		headi[2];
char		mar345=1;
extern void	get_pck(FILE *,WORD *);
extern void	swapint32(unsigned char *, int);
extern void 	swapint16(unsigned char *, int);

	/* Check input */
	if ( strlen( file) < 1 ) 
		usage();

	/* Allocate memory */
	i2_image = (unsigned short *)malloc( 3450*3450*sizeof(unsigned short));

	/* =============== Read and print image header =============== */

    	sprintf( str, " **************************************************\n\n");
	fprintf( stdout, str); 
    	sprintf( str, "      Program   : READMAR = reads a mar345 image\n");
	fprintf( stdout, str);
    	sprintf( str, "      Version   : %1.1f  (%s)\n",VERSION,__DATE__);
	fprintf( stdout, str);
    	sprintf( str, "      Author    : Claudio Klein, mar research (Hacked by FL)\n");
	fprintf( stdout, str);
    	sprintf( str, " **************************************************\n\n");  
	fprintf( stdout, str); 

	/* Open file */
	in  = fopen(  file,  "r+" );
	if ( !in  ) {
		printf( "readmar: Input file %s does not exist! \n",file);
		exit(-1);
	}

	/* Check if byte_swapping is necessary */
	fread( &h1, sizeof( int ), 1, in);
	if ( h1 == 1200 || h1 == 2000 || h1 == 1600 || h1 == 2300 || h1 == 3450 || h1 == 3000 || h1 == 2400 || h1 == 1800 || h1 == 2560 || h1 == 3072 ) {
		mar345 = 0;
		byteswap = 0;
	}
	else if ( h1 == 1234 ) {
		mar345   = 1;
		byteswap = 0;
	}
	else {
		byteswap = 1;
		swapint32( (unsigned char *)&h1, 1*sizeof(int) );
	}
	if ( byteswap ) {
		if ( h1 == 1234 ) {
			mar345 	= 1;
		}
		else if ( h1 > 10 && h1 <  5000  ) 
			mar345 = 0;
		else {
			printf("ERROR: Cannot swap first byte in header\n");
			exit(-1);
		}
	}

	if ( mar345 ) {
		h345 = Getmar345Header( in );
		nx = h345.size;
		no = h345.high;
		if ( h345.pixels != nx*nx && h345.pixels > 0 ) {
			ny   = h345.pixels/nx;
		}
		else {
			ny = h345.size;
		}

	}
	else {
		h300 = Getmar300Header( in );
		nx = h300.pixels_x;
		ny = h300.pixels_y;
		no = h300.high_pixels;
	}

	if ( nx < 10 || nx > 4000 || ny < 10 || ny > 4000 ||  no > 9999999 ) {
		printf("Something wrong with header: size is %d\n",nx);
	}

	memset( (char *)i2_image, 0, sizeof(short)*nx*ny );

	/* Read core of i2_image */
	/* Go to first record */
	if ( mar345 ) {
		fseek( in, 4096+FPOS(no), SEEK_SET );
		get_pck( in, i2_image);
	}
	else {
		fseek( in, nx+ny, SEEK_SET );
		i = fread( i2_image, sizeof( short ), nx*ny, in );
		if ( i != nx*ny ) 
			fprintf( stdout, "WARNING: read only %d out of %d pixels...\n",i,nx*ny);
		if (byteswap ) 
			swapint16(i2_image, nx*ny*sizeof(unsigned short));
	}

	/* There are some high intensity values stored in the image */
	if ( no ) {
		/* Allocate memory */
		ch_image = (unsigned char *)malloc( nx*ny*sizeof(char));

		/* Go to start of overflow record */
		i = fseek( in, 4096,   SEEK_SET );

		/* 
		 * Read overflows. Read a pair of values ( strong ), where
		 * first  value = address of pixel in i2_image array
		 * second value = intensity of pixel (32-bit) 
		 * In order to save memory, we don't allocate a 32-bit array
		 * for all data, but a 16-bit array and an 8-bit array.
		 * The final intensity 24-bit intensity is stored as:
		 * 24-bit-value = 16-bit-value + (8-bit-value)*65535;
		 * Note: the maximum intensity the scanner may produce is 250.000
		 *       Beyond the saturation of the ADC, all saturated pixels
		 *       get an intensity of 999.999 !
		 */ 
		for(i=0;i<no; i++ ) {
			j = fread( headi, sizeof(int), 2, in);

			if ( byteswap) 
				swapint32( headi, sizeof(int)*2 );

			iadd = headi[0];
			if ( iadd >= (nx*ny) || iadd < 0 ) continue;

			/* Count how many times the intensity fits into
			 * 65535
			 */
			while ( headi[ 1 ] > 65535 ) {
				headi[1] -= 65535;
				ch_image[iadd]++;
			}
			i2_image[iadd] = headi[1];
		}
	}

	/* Close input file */
	fclose( in );

	/* ======================= All done ======================== */
	//exit(0);


}

/******************************************************************
 * Function: usage
 ******************************************************************/
static void usage(void)
{
	printf( "\n      readmar [-h] file\n");
	printf(" e.g. readmar           xtal_001.mar2300\n\n");
	printf(" Command line options:\n");
	printf(" 	-h	help\n");

	//exit( 0 );
}
